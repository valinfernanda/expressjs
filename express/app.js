const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  // res.send('Hello World!')
  // res.json({
  //   nama: 'Valin',
  //   umur: 24  
  // })
  res.sendFile('./index.html', {root: __dirname})
})

app.get('/about', (req, res) => {
  // res.send('About Page!')
  res.sendFile('./about.html', {root: __dirname})

})

app.get('/contact', (req, res) => {
  // res.send('Contact Page!')
  res.sendFile('./contact.html', {root: __dirname})

})

app.get('/product/:id', (req, res)=>{
  res.send(`Product ID :  ${req.params.id} <br> Category : ${req.query.category}`)
})

app.use('/', (req, res) => {
  res.status(404)
  res.send('Test')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})


// const fs = require("fs");
// const http = require("http");


// http
//   .createServer((req, res) => {
//     res.writeHead(200, {
//       "Content-Type": "text/html",
//     });
// //
//     const url = req.url;
//     if (url === "/about") {
//       res.write("<h1>Hey Valin-about</h1>");
//       res.end();
//     } else if (url === "/contact") {
//       res.write("<h1>Hey Valin-contact</h1>");
//       res.end();
//     } else {
//       fs.readFile("./index.html", (err, data) => {
//         if (err) {
//           res.writeHead(404);
//           res.write("Error: file not found");
//         } else {
//           res.write(data);
//         }
//         res.end();
//       });
//     }
//   })
//   .listen(3000, () => {
//     console.log("Server is listening on port 3000...");
//   });
// //untuk menjalankan web server
