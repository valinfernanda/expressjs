const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const { loadContact, findContact } = require("./utils/contacts");

const app = express();
const port = 3000;

//gunakan view engine EJS
app.set("view engine", "ejs");

//third party middleware
app.use(expressLayouts);

//build-in middleware
app.use(express.static("public"));

app.get("/", (req, res) => {
  const mahasiswa = [
    {
      nama: "Laura Erika",
      email: "laura@gmail.com",
    },
    {
      nama: "Kevin William",
      email: "kevin@gmail.com",
    },
    {
      nama: "Valin Fernanda",
      email: "valin@gmail.com",
    },
  ];
  res.render("index", {
    nama: "VFEL",
    title: "Halaman Home",
    mahasiswa: mahasiswa,
    layout: "layouts/main-layout",
  });
});

app.get("/about", (req, res) => {
  res.render("about", {
    title: "Halaman About",
    layout: "layouts/main-layout",
  });
});

app.get("/contact", (req, res) => {
  const contacts = loadContact();
  // console.log(contacts)
  // res.render('contact')
  res.render("contact", {
    title: "Halaman Contact",
    layout: "layouts/main-layout",
    contacts: contacts,
  });
});

app.get("/contact/:nama", (req, res) => {
  const contact = findContact(req.params.nama);
  // console.log(contacts)
  // res.render('contact')
  res.render("detail", {
    title: "Halaman Detail Contact",
    layout: "layouts/main-layout",
    contact: contact,
  });
});

app.use("/", (req, res) => {
  res.status(404);
  res.send("Test");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
